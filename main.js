window.onload = (function(){
    var red, green, blue, app, div, button;
        app = document.getElementById('app');
        div = document.createElement('div');
        button = document.createElement('button');
    function getRandomIntInclusive(min, max) {
       min = Math.ceil(min);
       max = Math.floor(max);
       return Math.floor(Math.random() * (max - min + 1)) + min;
   }; // function get random int between min and max value
    function rgb(r,g,b) {
      return 'rgb('+(r + ',' + g + ',' + b)+')';
   }; // function create random color in format 'rgb(r,g,b)'
    function rrggbb(rr,gg,bb) {
       return '#' + rr.toString(16) + gg.toString(16) + bb.toString(16);
   }; // function create random color in format 'HEX' format
        red = getRandomIntInclusive(0,256);
        green = getRandomIntInclusive(0,256);
        blue = getRandomIntInclusive(0,256);
   //style for elements app, div,  button;
        app.style.background = rgb(red, green, blue);
        app.style.display = 'flex';
        app.style.justifyContent = 'center';
        app.style.alignItems = 'center';
        button.innerText = 'Click me to reload';
        button.setAttribute('onClick', 'document.location.reload()'); // create attribute onclick and reload page when clicked
        button.style.margin = '0 auto';
        button.style.display = 'block';
        app.appendChild(div).innerText = rrggbb(red, green, blue); // out text in div with value of color in a HEX format
        document.body.appendChild(button);
});
/*

  Задание 1.

  Написать функцию которая будет задавать СЛУЧАЙНЫЙ цвет для фона.
  Каждая перезагрузка страницы будет с новым цветом.
  Для написания используйте функцию на получение случайного целого числа,
  между минимальным и максимальным значением (Приложена снизу задания)

  + Бонус, повесить обработчик на кнопку через метод onClick
  + Бонус, использовать 6-ричную систему исчесления и цвет HEX -> #FFCC00
  + Бонус выводить полученый цвет по центру страницы.
  Необходимо создать блок через createElement задать ему стили через element.style
  и вывести через appendChild или insertBefore

  Необходимые материалы:
    Math.Random (Доки): https://developer.mozilla.org/uk/docs/Web/JavaScript/Reference/Global_Objects/Math/random
    function getRandomIntInclusive(min, max) {
      min = Math.ceil(min);
      max = Math.floor(max);
      return Math.floor(Math.random() * (max - min + 1)) + min;
    }
    __
    Работа с цветом:
    Вариант 1.
      Исользовать element.style.background = 'rgb(r,g,b)';
      где r,g,b случайное число от 0 до 256;

    Вариант 2.
      Исользовать element.style.background = '#RRGGBB';
      где, RR, GG, BB, значние цвета в 16-ричной системе исчесления
      Формирование цвета в вебе: https://ru.wikipedia.org/wiki/%D0%A6%D0%B2%D0%B5%D1%82%D0%B0_HTML
      Перевод в 16-ричную систему исчесления делается при помощи
      метода Number.toString( 16 ) https://www.w3schools.com/jsref/jsref_tostring_number.asp,

      var myNumber = '251'
      myNumber.toString(16) // fb

*/
